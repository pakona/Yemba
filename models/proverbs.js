var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var proverbsSchema = new Schema(
  {
    title: String,
    signification: String,
    audiourl: String
  }
);

mongoose.model('proverbs', proverbsSchema);
