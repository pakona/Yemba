var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  mongoose.model('proverbs').count((err, count) => {
    var rand = Math.floor(Math.random() * count);
    mongoose.model('proverbs').findOne().skip(rand).exec((err, proverb) => {
      res.render('index', {
        title: proverb.title,
        signification: proverb.signification,
        audiourl: proverb.audiourl
      });
    })
  })
});

router.get('/r', function(req, res, next) {
  mongoose.model('proverbs').count((err, count) => {
    var rand = Math.floor(Math.random() * count);
    mongoose.model('proverbs').findOne().skip(rand).exec((err, proverb) => {
      res.send(proverb);
    })
  })
});

module.exports = router;
