$(function(){
 $('#proverbRefresh').click(function(){
   $.get( '/r', {}, function(data) {
     $('#proverbTitle').html(data.title);
     $('#proverbSignification').html(data.signification);
   });
 });
});
